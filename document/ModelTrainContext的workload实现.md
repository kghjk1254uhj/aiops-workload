<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [ModelTrainContext的workload实现](#modeltraincontext%E7%9A%84workload%E5%AE%9E%E7%8E%B0)
  - [需求分析](#%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90)
  - [流程图](#%E6%B5%81%E7%A8%8B%E5%9B%BE)
  - [HONCON配置文件](#honcon%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)
  - [测试注意点](#%E6%B5%8B%E8%AF%95%E6%B3%A8%E6%84%8F%E7%82%B9)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# ModelTrainContext的workload实现

## 需求分析

将脚本按照key=value的形式进行切割，其中value可变，写入HOCON配置文件。

value写入HOCON配置文件之后，就能够将value的值通过workload加载到代码里面的变量中，这时候，将相关内容拼接成字符串，然后将字符串写入到本地文件中（本地文件所在目录在HOCON配置）。

接着调用生成的脚本，然后将脚本执行过程的时间记录，将脚本生成的时间记录，将任务编号以及任务名称都记录，作为输出。

## 流程图

流程图大致如下：

![](../images/process.png)

（写入之前，先判断run_train.sh是否存在，存在则删除。）

1.在workload中写一个方法，将脚本参数内容从HOCON获取并拼接成字符串，写入到run_train.sh

2.调用run_train.sh

3.这样子，HOCON修改，那么脚本就能够修改了。

## HONCON配置文件

```bash
spark-bench = {
  spark-submit-config = [{
    spark-args = {
      master = "yarn"
      driver-class-path = "/opt/spark-bench_2.3.0_0.4.0-RELEASE/lib/*"
    }
    workload-suites = [{
      descr = "Module train context pressure test"
      benchmark-output = "console"
      workloads = [
        {
          name = "custom"
          class = "com.bonree.ai.contextpt.ModelTrainContextPT"  // 执行的workload对象
          clazz = "com.bonree.ai.context.ModelTrainContext"  // 应用程序执行的主类
          // input = "/opt/aiops/conf/config.properties"  // 训练之后模型存放的地址，可以用parquet或者csv的格式进行数据落地[暂定]
          output = "console"  // 输出的位置
          master = "yarn"  // 提交任务的master地址
          deploy-mode = "client"  // 在本地（client）启动driver或在cluster上启动，默认是client
          conf = "spark.default.parallelism=12"
          driver-memory = "1g"  // driver内存，默认1G
          driver-cores = 2  // driver的核数，默认是1，在yarn或者standalone下使用
          executor-memory = "3g"  // 每个executor的内存，默认是1G
          executor-cores = 4  // 每个executor的核数。在yarn或者standalone下使用
          num-executors = 2
          app-dir = "/data/bonree_3/main_jars/BonreeAIOps.jar"  // BonreeAIOps.jar存放以及spark-submit脚本生成的地址
          hdfs-addr = "hdfs://ai01:9000"  // hdfs的地址
          shell-dir = "/data/bonree_3/script"  // 脚本名称
          shell-name = "run_train.sh"  // 表示脚本名称
          config-path = "/data/bonree_3/conf/config.properties"  // 表示从hdfs的位置读取配置文件
        }
      ]
    }]
  }]
}
```

## 测试注意点

需要修改BonreeAIOps中的代码

ModelTrainContext中，需要修改的属性：

- val appName需要修改为自己的固定字符串
- val modelDir需要在配置中进行修改，避免跟之前训练后生成的模型冲突，配置修改之后重新上传到HDFS中即可
- 对应的ModelTrainTask，注释掉send(modelConfigs)，避免将模型配置发送到redis中，影响之前的数据

修改之后，需要重新编译BonreeAIOps，如图所示：

![](../images/compiler.png)