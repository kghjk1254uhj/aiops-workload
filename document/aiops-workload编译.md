<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [aiops-workload编译](#aiops-workload%E7%BC%96%E8%AF%91)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# aiops-workload编译

编译aiops-workload，执行：

```xml
$ mvn -X clean package -Dmaven.test.skip=true
```

编译之后会在项目根目录下生成target/文件夹，里面生成的jar包就是所需要的。