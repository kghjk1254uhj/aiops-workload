#!/bin/sh
INSTALL_DIR=/data/bonree

path=$INSTALL_DIR/conf/config.properties

echo $path

spark-submit \
--class com.bonree.ai.AppTest \
--master yarn \
--deploy-mode client \
--driver-memory 1g \
--driver-cores 1 \
--executor-memory 1g \
--executor-cores 1 \
$INSTALL_DIR/jar/BonreeAIOps-1.0-SNAPSHOT.jar \
hdfs://ai01:9000/ /data/bonree/conf/config.properties 100 30340
