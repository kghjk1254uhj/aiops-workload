#!/bin/sh
INSTALL_DIR=/data/bonree

path=$INSTALL_DIR/conf/config.properties

echo $path

spark-submit \
--class com.bonree.ai.context.EventTranStreamingContext \
--master yarn \
--deploy-mode cluster \
--driver-memory 1g \
--driver-cores 2 \
--executor-memory 3g \
--executor-cores 3 \
$INSTALL_DIR/jar/BonreeAIOps-1.0-SNAPSHOT.jar \
hdfs://ai01:9000 /data/bonree/conf/config.properties
