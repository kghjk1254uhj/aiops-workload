#!/bin/sh
INSTALL_DIR=/data/bonree

path=$INSTALL_DIR/conf/config.properties

export JAVA_HOME=/usr/lib/java-1.8.0/java-1.8.0-openjdk
export SPARK_HOME=/usr/local/spark
export HADOOP_HOME=/usr/local/hadoop
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin
export PATH=$PATH:/usr/local/scala/bin
echo $path

$SPARK_HOME/bin/spark-submit \
--class com.bonree.ai.context.TrainDataFetchContext \
--master yarn \
--deploy-mode client \
--driver-memory 1g \
--driver-cores 2 \
--executor-memory 2g \
--executor-cores 2 \
$INSTALL_DIR/jar/BonreeAIOps-1.0-SNAPSHOT.jar \
hdfs://ai01:9000 /data/bonree/conf/config.properties
