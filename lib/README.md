<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [文件夹说明](#%E6%96%87%E4%BB%B6%E5%A4%B9%E8%AF%B4%E6%98%8E)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 文件夹说明

本文件夹下，存放的是SparkBench的两个编译之后的两个jar包：

```bash
spark-bench-2.3.0_0.4.0-RELEASE.jar
spark-bench-launch-2.3.0_0.4.0-RELEASE.jar
```

两个SparkBench的包是workload必须调用到的。

将两个文件夹放到此处之后，还需要在pom.xml文件中，导入这两个jar包到项目中，具体操作详见：pom.xml文件。