/**
 * Copyright (C), 2019, hchihping
 * Title: AssertTest
 * Author: hchihping
 * Date: 2019/12/10 15:54
 */
package com.bonree.ai.test

import java.io.{File, PrintWriter}

import com.bonree.ai.record.MetricRecord
import org.apache.log4j.Logger

import scala.util.Random


object AssertTest {
  private val logRoot: Logger = Logger.getRootLogger
  private val log1: Logger = Logger.getLogger("log1")

  def createShellScript(file: File): Unit = {
    if (file.exists()) {
      file.delete
    }

    // Simulation to obtain the corresponding parameter value of HOCON file.
    val clazz = "com.bonree.ai.context.ModelTrainContext"
    val master = "yarn"
    val deployMode = "client"
    val conf = "spark.default.parallelism=12"
    val driverMemory = "1g"
    val driverCores = "2"
    val executorMemory = "3g"
    val executorCores = "4"
    val appDir = "/opt/main_jars/BonreeAIOps.jar"
    val hdfsAddress = "hdfs://ai01:9000"
    val configPath = "/opt/aiops/conf/config.properties"
    val numExecutors = "4"

    // Concat the script.
    val head = "#!/bin/sh" + "\n"
    val sp = "spark-submit \\" + "\n"
    val cl = s"--class $clazz \\" + "\n"
    val ma = s"--master $master \\" + "\n"
    val de = s"--deploy-mode $deployMode \\" + "\n"
    val co = s"--conf $conf \\" + "\n"
    val dm = s"--driver-memory $driverMemory \\" + "\n"
    val dc = s"--driver-cores $driverCores \\" + "\n"
    val em = s"--executor-memory $executorMemory \\" + "\n"
    val ec = s"--executor-cores $executorCores \\" + "\n"
    val ne = s"--num-executors $numExecutors \\" + "\n"
    val rd = s"$appDir \\" + "\n"
    val hd = s"$hdfsAddress \\" + "\n"
    val in = s"$configPath" + "\n"
    // Concat the script.
    val shell = head + sp + cl + ma + de + co + dm + dc + em + ec + ne + rd + hd + in

    // Generate the script.
    log1.info("Script generating.")
    val out = new PrintWriter(file)
    out.write(shell)
    out.flush()
    out.close()
    log1.info("Script generated.")
  }

  /**
   * Test generate MetricRecord data.
   *
   * @return MetricRecord
   */
  def testDataGen(): MetricRecord = {
    // ids---->Seq[(String, Int)]
    val ids = Seq(("mid", Random.nextInt(10000)), ("pid", Random.nextInt(10000)), ("apid", Random.nextInt(10000)), ("btid", Random.nextInt(10000)))

    // content---->Map(String, Int)
    // Random string.
    // val str = org.apache.commons.lang.RandomStringUtils.random(32)
    var str:String = BigInt.probablePrime(100,scala.util.Random).toString(16)
    val content: Map[String, Int] = Map(str -> Random.nextInt(10000))

    // timestamp---->Long
    val timestamp = 1000L
    val metricRecord = new MetricRecord(ids, content, timestamp)
    metricRecord
  }

  def main(args: Array[String]): Unit = {
    ///////////////////////////////////////////////////////////////////////////////////////
    // Test create shell script in local path.
    ///////////////////////////////////////////////////////////////////////////////////////
    // val file = new File("run_train.sh")
    // createShellScript(file)

    ///////////////////////////////////////////////////////////////////////////////////////
    // Test generate data.
    ///////////////////////////////////////////////////////////////////////////////////////
    println(testDataGen())

    ///////////////////////////////////////////////////////////////////////////////////////
    // Test concat the link.
    ///////////////////////////////////////////////////////////////////////////////////////
    // val hdfsAddress = "hdfs://ai01:9000"

    // val saveDir = "/data/bonree_3/1/2019-11-12/"

    // print(s"$hdfsAddress$saveDir")

    ///////////////////////////////////////////////////////////////////////////////////////
    // Test concat Seq.
    ///////////////////////////////////////////////////////////////////////////////////////
    /*
    val string_time = "2014-09-00 00:00:00"
    var i = 1
    var j = 0
    for (i <- 1 to 2) {
      var seq: Seq[Int] = Seq(1)
      for (j <- 1 to 1440) {
        seq ++= Some(j)
      }
      println(seq)
    }
    */
  }
}
