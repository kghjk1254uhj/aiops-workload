/**
 * Copyright (C), 2019, http://www.bonree.com
 * Title: TrainDataFetchContextPT
 * Author: hchihping
 * Date: 2019/12/16 11:53
 */
package com.bonree.ai.contextpt

import java.io.File

import com.bonree.ai.utils.WorkloadUtils._
import com.ibm.sparktc.sparkbench.utils.GeneralFunctions.{getOrDefault, getOrThrow, _}
import com.ibm.sparktc.sparkbench.utils.SaveModes
import com.ibm.sparktc.sparkbench.workload.{Workload, WorkloadDefaults}
import org.apache.spark.sql.{DataFrame, SparkSession}

/** Create the case class to return the train result. */
case class ModelTrainContextPTResult(
                                      name: String,
                                      appId: String,
                                      timestamp: Long,
                                      create_shell_time: Long,
                                      train_time: Long,
                                      total_time: Long
                                    )


/** Get the content of configuration from HOCON config file. */
object ModelTrainContextPT extends WorkloadDefaults {
  override val name: String = "module-train"

  override def apply(m: Map[String, Any]): Workload = ModelTrainContextPT(
    output = Some(getOrThrow(m, "output").asInstanceOf[String]),
    clazz = getOrThrow(m, "clazz").asInstanceOf[String],
    master = getOrThrow(m, "master").asInstanceOf[String],
    deployMode = getOrThrow(m, "deploy-mode").asInstanceOf[String],
    conf = getOrThrow(m, "conf").asInstanceOf[String],
    driverMemory = getOrThrow(m, "driver-memory").asInstanceOf[String],
    driverCores = getOrThrow(m, "driver-cores").asInstanceOf[Int],
    executorMemory = getOrThrow(m, "executor-memory").asInstanceOf[String],
    executorCores = getOrThrow(m, "executor-cores").asInstanceOf[Int],
    numExecutors = getOrThrow(m, "num-executors").asInstanceOf[Int],
    appDir = getOrThrow(m, "app-dir").asInstanceOf[String],
    hdfsAddress = getOrThrow(m, "hdfs-addr").asInstanceOf[String],
    shellDir = getOrThrow(m, "shell-dir").asInstanceOf[String],
    shellName = getOrThrow(m, "shell-name").asInstanceOf[String],
    configPath = getOrThrow(m, "config-path").asInstanceOf[String],
    saveMode = getOrDefault[String](m, "save-mode", SaveModes.error)
  )
}


/** The case class of workload, parameters can refer to the HOCON configuration file. */
case class ModelTrainContextPT(
                                input: Option[String] = None,
                                output: Option[String],
                                clazz: String,
                                master: String,
                                deployMode: String,
                                conf: String,
                                driverMemory: String,
                                driverCores: Int,
                                executorMemory: String,
                                executorCores: Int,
                                numExecutors: Int,
                                appDir: String,
                                hdfsAddress: String,
                                shellDir: String,
                                shellName: String,
                                configPath: String,
                                saveMode: String
                              ) extends Workload {

  override def doWorkload(df: Option[DataFrame], sparkSession: SparkSession): DataFrame = {
    val start_time = System.currentTimeMillis()
    val head = "#!/bin/sh" + "\n"
    val sp = "spark-submit \\" + "\n"
    val cl = s"--class $clazz \\" + "\n"
    val ma = s"--master $master \\" + "\n"
    val de = s"--deploy-mode $deployMode \\" + "\n"
    val co = s"--conf $conf \\" + "\n"
    val dm = s"--driver-memory $driverMemory \\" + "\n"
    val dc = s"--driver-cores $driverCores \\" + "\n"
    val em = s"--executor-memory $executorMemory \\" + "\n"
    val ec = s"--executor-cores $executorCores \\" + "\n"
    val ne = s"--num-executors $numExecutors \\" + "\n"
    val rd = s"$appDir \\" + "\n"
    val hd = s"$hdfsAddress \\" + "\n"
    val in = s"$configPath" + "\n"
    // concat the script
    val shell = head + sp + cl + ma + de + co + dm + dc + em + ec + ne + rd + hd + in

    println(s"================>Generate the $shellName script and grant permissions.")
    val (create_shell_time, file) = time {
      /** Create the script file here, and the user default is root, or will cause "chown" problem. */
      val file = new File(s"$shellDir/$shellName")
      createShellScript(file, shell)
      import scala.sys.process._
      val chmod = s"chmod +x $file".!!
    }
    println(s"================>Generated the $shellName script and grant over.")

    println(s"================>$shellName script begin.")
    val (train_time, result) = time {
      val result = s"$shellDir/$shellName"
      import scala.sys.process._
      s"$result > /data/bonree/log/train_3.log 2>&1 &".!!
    }
    println(s"================>$shellName script end.")

    val total_time = create_shell_time + train_time

    sparkSession.createDataFrame(Seq(ModelTrainContextPTResult(
      "module-train",
      sparkSession.sparkContext.applicationId,
      start_time,
      create_shell_time,
      train_time,
      total_time
    )))
  }

}