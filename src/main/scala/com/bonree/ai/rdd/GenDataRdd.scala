/**
 * Copyright (C), 2020, hchihping
 * Title: GenDataRdd
 * Author: hchihping
 * Date: 2020/1/8 12:02
 * Description: An RDD is a day of data.
 */
package com.bonree.ai.rdd

import com.bonree.ai.record.MetricRecord
import org.apache.spark.{Partition, SparkContext, TaskContext}
import org.apache.spark.rdd.RDD

import scala.util.Random

class GenDataRdd(sc: SparkContext, val infos: Seq[GendDataPartitionInfo])
  extends RDD[MetricRecord](sc, Nil) {

  import GenDataRdd._

  override def getPartitions: Array[Partition] = {
    infos.zipWithIndex.map { case (info, i) =>
      new GenDataRddPartition(i, info)
    }.toArray
  }

  override def compute(split: Partition, context: TaskContext): Iterator[MetricRecord] = {
    val part = split.asInstanceOf[GenDataRddPartition]
    val GendDataPartitionInfo(begin, end, gran, num, id) = part.info
    GenDataRdd.getRecords(begin, end, gran, num, id)
  }

}

object GenDataRdd {

  def genData(ids: Seq[(String, Int)], ts: Long): MetricRecord = {
    val content = Map(
      "a" -> Random.nextInt(10000),
      "b" -> Random.nextInt(10000),
      "c" -> Random.nextInt(10000))
    val timestamp = ts
    MetricRecord(ids, content, timestamp)
  }

  def getRecords(begin: Long, end: Long, gran: Long, num: Int, id: String): Iterator[MetricRecord] = {
    val records = (begin until end by gran).flatMap { t =>
      (0 until num).map { i =>
        this.genData(Seq(id -> i), t)
      }
    }
    Random.shuffle(records) // 打乱数据
    records.toIterator
  }

  class GenDataRddPartition(val index: Int, val info: GendDataPartitionInfo) extends Partition {}

}
