/**
 * Copyright (C), 2020, hchihping
 * Title: GendDataConfig
 * Author: hchihping
 * Date: 2020/1/9 14:00
 */
package com.bonree.ai.rdd

case class GendDataPartitionInfo(begin: Long, end: Long, gran: Long, num: Int, id: String) {}
