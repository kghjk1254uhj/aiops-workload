/**
 * Copyright (C), 2019, http://www.bonree.com
 * Title: HdfsUtils
 * Author: hchihping
 * Date: 2019/12/30 15:54
 */
package com.bonree.ai.utils

import java.io.{ByteArrayOutputStream, FileNotFoundException, ObjectOutputStream}

import com.bonree.ai.record.MetricRecord
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FSDataOutputStream, FileSystem, Path}

private[bonree] object HdfsUtils {

  val conf = new Configuration()
  var fs: FileSystem = _

  /**
   * Connect to FileSystem.
   *
   * @param url  FileSystem url, for example, hdfs://ai01:9000
   * @param path File path in HDFS, for example, /data/bonree_3/conf/config.properties
   * @return
   */
  def getFileSystem(url: String, path: String): FileSystem = {
    conf.set("fs.defaultFS", url)
    new Path(path).getFileSystem(conf)
  }

  /** Decide directory is exists in HDFS or not. */
  def isExists(url: String, path: String): Boolean = {
    getFileSystem(url, path).exists(new Path(path))
  }

  /** Save data to path in HDFS. */
  def saveData(data: MetricRecord, url: String, path: String): Unit = {
    val fs = getFileSystem(url, path)
    // Change any object to byteArray.
    val outputStream: FSDataOutputStream = fs.create(new Path(path))
    val bos: ByteArrayOutputStream = new ByteArrayOutputStream()
    val oos: ObjectOutputStream = new ObjectOutputStream(bos)
    oos.writeObject(data)
    oos.flush
    val ba = bos.toByteArray
    outputStream.write(ba)
    outputStream.close()
  }

  /** Delete file(Directory) if file(Directory) is exits. */
  def deletePath(url: String, path: String): Boolean = {
    try {
      val filesys = getFileSystem(url, path)
      filesys.delete(new Path(path), true)
    } catch {
      case ex: FileNotFoundException => {
        println("File is not exits.")
        false
      }
    }
  }
}
