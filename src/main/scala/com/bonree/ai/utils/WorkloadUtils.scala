/**
 * Copyright (C), 2019, http://www.bonree.com
 * Title: WorkloadUtils
 * Author: hchihping
 * Date: 2019/12/14 15:54
 */
package com.bonree.ai.utils

import java.io.{File, FileNotFoundException, PrintWriter}
import java.text.SimpleDateFormat
import java.util.Properties

import com.bonree.ai.record.MetricRecord
import com.bonree.ai.utils.HdfsUtils._
import org.apache.hadoop.fs.{Path, FSDataInputStream => FSData}

import scala.util.Random

private[bonree] object WorkloadUtils {

  /** Get the parameter of HOCON config file, write the value to the script. */
  def createShellScript(file: File, shell: String): Unit = {
    println("================>WorkloadUtils.")
    if (file.exists()) {
      println("================>Execute delete operation, because " + file.toString + " is exists.")
      file.delete
      println("================>File " + file.toString + " was deleted.")
    }
    val out = new PrintWriter(file)
    out.write(shell)
    out.flush()
    out.close()
  }

  /**
   * Read configuration item from config file path, these two params should be defined as configuration items.
   *
   * @url HDFS address, for example, hdfs://ai01:9000.
   * @param configPath The directory where the file config.properties is located. For example, /data/bonree_3/conf.
   * @param configItem The key of the file config.properties. For example, basic_hadoop_train_data_path.
   * @return
   */
  def readConfig(url: String, configPath: String, configItem: String): String = {
    var configItemValue: String = null
    if (isExists(url, configPath) == true) {
      val props: Properties = new Properties()
      val inputStream: FSData = getFileSystem(url, configPath).open(new Path(configPath))
      props.load(inputStream)
      configItemValue = props.getProperty(configItem)
    } else {
      throw new FileNotFoundException(s"File $configPath is not exits.")
    }
    configItemValue
  }

  /** Generate MetricRecord type data. */
  def genData(ts: Long): Seq[MetricRecord] = {
    val ids = Seq(
      ("mid", Random.nextInt(10000)),
      ("pid", Random.nextInt(10000)),
      ("apid", Random.nextInt(10000)),
      ("btid", Random.nextInt(10000))
    )
    var str: String = BigInt.probablePrime(100, scala.util.Random).toString(16)
    val content = Map(str -> Random.nextInt(10000))
    val timestamp = ts
    val metricRecord = MetricRecord(ids, content, timestamp)
    Seq(metricRecord)
  }


  /** Get the timestamp of 00:00:00 of the day based on date-> ms */
  def getDateTimeStamp(date: String): Long = {
    val fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val dt = fm.parse(date)
    val timestamp = dt.getTime
    timestamp
  }

  /** Convert timestamp to time format, and intercept date to return. */
  def ts2Date(timestamp: Long): String = {
    new SimpleDateFormat("yyyy-MM-dd").format(timestamp)
  }
}
