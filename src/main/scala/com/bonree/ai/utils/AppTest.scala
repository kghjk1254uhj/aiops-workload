/**
 * Copyright (C), 2020, http://www.bonree.com
 * Title: AppTest
 * Author: hchihping
 * Date: 2020/1/7 11:17
 */
package com.bonree.ai.utils

import com.bonree.ai.record.MetricRecord
import com.bonree.ai.utils.WorkloadUtils.genData
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Random

object AppTest {
  def main(args: Array[String]): Unit = {
    ///////////////////////////////////////////////////////////////////////////////////////
    // Test read config from configuration file of HDFS.
    ///////////////////////////////////////////////////////////////////////////////////////
    /*
    val hdfsAddress = "hdfs://ai01:9000"
    val configPath = "/data/bonree_3/conf/config.properties"
    val configItem = "basic_hadoop_train_data_path"
    // Test method readConfig().
    val value = readConfig("hdfs://ai01:9000", "/data/bonree_3/conf/config.properties", configItem)
    println(s"================>ConfigItem $configItem value is " + value)
    */

    ///////////////////////////////////////////////////////////////////////////////////////
    // Test save RDD[MetricRecord] to HDFS, saveData method, etc.
    ///////////////////////////////////////////////////////////////////////////////////////
    /*
    val ids = Seq(
      ("mid", Random.nextInt(10000)),
      ("pid", Random.nextInt(10000)),
      ("apid", Random.nextInt(10000)),
      ("btid", Random.nextInt(10000))
    )
    val str = org.apache.commons.lang.RandomStringUtils.random(32)
    val content: Map[String, Int] = Map(str -> Random.nextInt(10000))
    val timestamp = 1000L
    val metricRecord = new MetricRecord(ids, content, timestamp)

    Seq(metricRecord)

    val conf = new SparkConf().setMaster("local[*]").setAppName("data_gen")

    val sc = new SparkContext(conf)

    val mr_rdd = sc.parallelize(genData(1000L))

    val hdfsAddress = "hdfs://ai01:9000"

    val saveDir = "/data/bonree_3/1/2019-11-13/"

    print("================>" + s"$hdfsAddress$saveDir")

    mr_rdd.saveAsObjectFile(s"$hdfsAddress$saveDir")

    // saveData(metricRecord, hdfsAddress, "/data/bonree_3/testsavemr.txt")
    */

    ///////////////////////////////////////////////////////////////////////////////////////
    // Test isExists().
    ///////////////////////////////////////////////////////////////////////////////////////
    /*
    val b1 = HdfsUtils.isExists("hdfs://ai01:9000", "/data/bonree_3/train_data/1/2019-12-13") // Value is true.
    val b2 = HdfsUtils.isExists("hdfs://ai01:9000", "/data/bonree_3/train_data/1") // Value is true.
    val b3 = HdfsUtils.isExists("hdfs://ai01:9000", "/data/bonree_41") // Value is false.
    println("=======>b1: " + b1)
    println("=======>b2: " + b2)
    println("=======>b3: " + b3)
    */

    ///////////////////////////////////////////////////////////////////////////////////////
    // Test deletePath().
    ///////////////////////////////////////////////////////////////////////////////////////
    /*
    val b1 = HdfsUtils.isExists("hdfs://ai01:9000", "/data/bonree_3/train_data/1/2019-12-13")
    println("=======>b1: " + b1)
    HdfsUtils.deletePath("hdfs://ai01:9000", "/data/bonree_3/train_data/1/2019-12-13")
    val b4 = HdfsUtils.isExists("hdfs://ai01:9000", "/data/bonree_3/train_data/1/2019-12-13")
    println("=======>b4: " + b4)
    */
  }
}
