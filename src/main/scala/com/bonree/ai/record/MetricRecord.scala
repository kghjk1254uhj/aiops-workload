/**
 * Copyright (C), 2019, http://www.bonree.com
 * Title: MetricRecord
 * Author: hchihping
 * Date: 2019/12/31 14:05
 */
package com.bonree.ai.record

@SerialVersionUID(-8071784259538508888L)
private[ai] class MetricRecord(
                                val ids: Seq[(String, Int)],
                                val content: Map[String, Int],
                                val timestamp: Long
                              ) extends Serializable {
  final def key: String = ids.map(x => s"${x._1}=${x._2}").mkString("_")

  def metricType: String = "null"

  def copy(
            ids: Seq[(String, Int)] = null,
            content: Map[String, Int] = null,
            timestamp: Long = 0L
          ): MetricRecord = {
    val _ids: Seq[(String, Int)] = if (ids == null) this.ids else ids
    val _content: Map[String, Int] = if (content == null) this.content else content
    val _timestamp: Long = if (timestamp <= 0L) this.timestamp else timestamp
    MetricRecord(_ids, _content, _timestamp)
  }

  def granularity: Long = 60000L

  override def toString: String = {
    s"${metricType}MetricRecord($key, content=$content, timestamp=$timestamp, granularity=$granularity)"
  }

}

@SerialVersionUID(-8071784259538508888L)
private[bonree] object MetricRecord {
  def apply(
             ids: Seq[(String, Int)],
             content: Iterable[(String, Int)],
             timestamp: Long
           ): MetricRecord = new MetricRecord(ids, content.toMap, timestamp)

  def unapply(mr: MetricRecord): Option[(Seq[(String, Int)], Map[String, Int], Long, Long)] = {
    if (mr == null) None else Some((mr.ids, mr.content, mr.timestamp, mr.granularity))
  }
}
