/**
 * Copyright (C), 2019, http://www.bonree.com
 * Title: ModelTrainDataGen
 * Author: hchihping
 * Date: 2019/12/25 14:08
 * Description: Generate data for ModelTrainContext. Can refer to LinearRegressionDataGen workload.
 */
package com.bonree.ai.datageneration

import com.bonree.ai.rdd.{GenDataRdd, GendDataPartitionInfo}
import com.bonree.ai.utils.{HdfsUtils, WorkloadUtils}
import com.ibm.sparktc.sparkbench.utils.GeneralFunctions._
import com.ibm.sparktc.sparkbench.utils.SaveModes
import com.ibm.sparktc.sparkbench.workload.{Workload, WorkloadDefaults}
import org.apache.spark.sql.{DataFrame, SparkSession => sparkSession}

/** Create the case class to return the report after generating data. */
case class ModelTrainDataGenResult(
                                    name: String,
                                    appId: String,
                                    timestamp: Long,
                                    generate_time: Long
                                  )

/** Construct DataFrame and then write to disk, can refer to LinearRegressionDataGen workload. */
case class ModelTrainDataGen(
                              input: Option[String] = None,
                              output: Option[String],
                              hdfsPath: String,
                              beginTime: String,
                              days: Int,
                              url: String,
                              partition: Int,
                              num: Int,
                              configPath: String,
                              configItem: String,
                              saveMode: String
                            ) extends Workload {

  /** Save Train data by RDD */
  private def saveDataByRdd(df: Option[DataFrame], sparkSession: sparkSession): DataFrame = {
    val ID_LIST = "ABCDEFGHIJKLMNOPQRST"
    val start_time = System.currentTimeMillis()
    import java.text.SimpleDateFormat
    val beginMill = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(beginTime).getTime
    val DAY_GRAN: Long = 24 * 60 * 60000
    val DATA_GRAN: Long = 60000
    // NUM & PARTITION can be changed, when NUM = 36, partition = 10, the amount of data is about 1G.
    val NUM: Int = num
    println("============>Data num: " + NUM)
    val PARTITION: Int = partition
    println("============>Partition: " + PARTITION)
    val parentDir = WorkloadUtils.readConfig(url, configPath, configItem)
    val idList = ID_LIST.take(partition).toCharArray.toSeq
    import scala.util.Random
    val (generate_time, strrdd) = time {
      for (i <- 0 until days) {
        val begin = beginMill + i * DAY_GRAN
        val end = begin + DAY_GRAN
        // Shuffle id list
        Random.shuffle(idList)
        val infos = (0 until partition).map { i =>
          val id = idList(i).toString
          GendDataPartitionInfo(begin, end, DATA_GRAN, NUM, id)
        }
        val rdd = new GenDataRdd(sparkSession.sparkContext, infos)
        val subDir = WorkloadUtils.ts2Date(begin)
        val saveDir = s"$url" + s"$parentDir" + "1/" + s"$subDir"
        // Save data to HDFS.
        HdfsUtils.deletePath(url, saveDir)
        rdd.saveAsObjectFile(s"$saveDir")
      }
    }

    sparkSession.createDataFrame(Seq(ModelTrainDataGenResult(
      "data-gen",
      sparkSession.sparkContext.applicationId,
      start_time,
      generate_time
    )))

  }

  override def doWorkload(df: Option[DataFrame], sparkSession: sparkSession): DataFrame = {
    this.saveDataByRdd(df, sparkSession)
  }
}

/** Generate the train data in /data/bonree_3/train_data/ of HDFS. */
object ModelTrainDataGen extends WorkloadDefaults {
  override val name: String = "model-train-data-generate"

  override def apply(m: Map[String, Any]): Workload = ModelTrainDataGen(
    output = Some(getOrThrow(m, "output").asInstanceOf[String]),
    hdfsPath = getOrThrow(m, "hdfs-path").asInstanceOf[String],
    beginTime = getOrThrow(m, "begin-time").asInstanceOf[String],
    days = getOrThrow(m, "days").asInstanceOf[Int],
    url = getOrThrow(m, "url").asInstanceOf[String],
    partition = getOrThrow(m, "partition").asInstanceOf[Int],
    num = getOrThrow(m, "num").asInstanceOf[Int],
    configPath = getOrThrow(m, "config-path").asInstanceOf[String],
    configItem = getOrThrow(m, "config-item").asInstanceOf[String],
    saveMode = getOrDefault[String](m, "save-mode", SaveModes.error)
  )
}